﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionalConveyor4 : MonoBehaviour
{
    public GameObject belt;
    public Transform endpoint4, endpoint1, endpoint2, endpoint3;
    public float speed;


    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<CubeSc>().activeBelt4 == 1)
        {
            other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint1.position, speed * Time.deltaTime);//v.(starrpoint, targetPoint, speed)
        }
        else if (other.GetComponent<CubeSc>().activeBelt4 == 2)
        {
            other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint2.position, speed * Time.deltaTime);//v.(starrpoint, targetPoint, speed)
        }
        else if (other.GetComponent<CubeSc>().activeBelt4 == 3)
        {
            other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint3.position, speed * Time.deltaTime);//v.(starrpoint, targetPoint, speed)
        }
        else if (other.GetComponent<CubeSc>().activeBelt4 == 4)
        {
            other.transform.position = Vector3.MoveTowards(other.transform.position, endpoint4.position, speed * Time.deltaTime);//v.(starrpoint, targetPoint, speed)
        }

    }
}
